import {Component} from '@angular/core';
import {RouterOutlet} from '@angular/router';
import {BpmnDesignerComponent} from "./components/bpmn-designer/bpmn-designer.component";

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, BpmnDesignerComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'flowable-modeler-angular';
}
