import { Component, Input } from '@angular/core';
import { ModdleElementPanelConfig, panelConfigs } from '@site/app/config/panel.config';
import { IconifyComponent } from '@site/app/components/icon/iconify/iconify.component';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NgIf } from '@angular/common';

@Component({
  selector: 'app-bpmn-panel-header',
  standalone: true,
  imports: [
    IconifyComponent,
    NzIconModule,
    NgIf
  ],
  templateUrl: './bpmn-panel-header.component.html',
  styleUrl: './bpmn-panel-header.component.css'
})
export class BpmnPanelHeaderComponent {

  config?: ModdleElementPanelConfig;

  @Input('type') set type(type: string) {
    console.log(type);
    if (type) {
      const _config = panelConfigs[type];
      if (_config) {
        this.config = _config;
      }

      console.log(this.config);
    }
  }
}
