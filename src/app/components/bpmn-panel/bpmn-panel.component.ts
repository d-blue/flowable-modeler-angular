import {Component, ElementRef, Input, Renderer2} from '@angular/core';
import {CommonModule} from "@angular/common";
import {NzIconModule} from "ng-zorro-antd/icon";
import {DEFAULT_LABEL_SIZE} from "bpmn-js/lib/util/LabelUtil";
import width = DEFAULT_LABEL_SIZE.width;
import { BpmnModelerService } from '@site/app/service/bpmn-modeler.service';
import { ElementChangeEvent } from '@site/typing/bpmn.define';
import {
  BpmnPanelHeaderComponent
} from '@site/app/components/bpmn-panel/bpmn-panel-header/bpmn-panel-header.component';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { GenericFormComponent } from '@site/app/components/bpmn-panel/children/generic-form/generic-form.component';

@Component({
  selector: 'app-bpmn-panel',
  standalone: true,
  imports: [
    CommonModule,

    NzIconModule,
    NzCollapseModule,

    BpmnPanelHeaderComponent,
    GenericFormComponent
  ],
  templateUrl: './bpmn-panel.component.html',
  styleUrl: './bpmn-panel.component.css'
})
export class BpmnPanelComponent {

  @Input('width') width = '900px';
  currentElement?: BpmnElement;
  type?: string = 'bpmn:Process';


  constructor(private el: ElementRef, private render: Renderer2, private bpmnModelerService: BpmnModelerService) {
    //this.bpmnModelerService.
    this.bpmnModelerService.addElementChangedListener((event) => this.onSelectElementChanged(event))
  }

  onSelectElementChanged(event: ElementChangeEvent) {
    this.currentElement = event.element;
    this.type = event.type;
  }
}
