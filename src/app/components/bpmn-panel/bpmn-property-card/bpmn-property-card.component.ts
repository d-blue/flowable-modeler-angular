import { Component, Input, TemplateRef } from '@angular/core';
import { IconifyComponent } from '@site/app/components/icon/iconify/iconify.component';
import { isTemplateRef } from 'ng-zorro-antd/core/util';
import { NgIf, NgTemplateOutlet } from '@angular/common';

@Component({
  selector: 'app-bpmn-property-card',
  standalone: true,
  imports: [
    IconifyComponent,
    NgIf,
    NgTemplateOutlet
  ],
  templateUrl: './bpmn-property-card.component.html',
  styleUrl: './bpmn-property-card.component.css'
})
export class BpmnPropertyCardComponent {

  @Input('icon') icon?: string | TemplateRef<any>;
  @Input('name') title?: string;
  protected readonly isTemplateRef = isTemplateRef;

  getIconTemplate(): TemplateRef<any> {
    return this.icon as TemplateRef<any>;
  }
  getIconString(): string {
    return this.icon as string;
  }
}
