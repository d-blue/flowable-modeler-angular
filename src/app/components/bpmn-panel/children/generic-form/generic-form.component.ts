import { Component } from '@angular/core';
import { FormGroup, FormsModule, NonNullableFormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import {
  BpmnPropertyCardComponent
} from '@site/app/components/bpmn-panel/bpmn-property-card/bpmn-property-card.component';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { IconifyComponent } from '@site/app/components/icon/iconify/iconify.component';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzIconModule } from 'ng-zorro-antd/icon';

@Component({
  selector: 'app-generic-form',
  standalone: true,
  imports: [
    FormsModule,
    ReactiveFormsModule,
    BpmnPropertyCardComponent,
    NzCollapseModule,
    NzFormModule,
    NzInputModule,
    NzLayoutModule,
    NzIconModule,
    IconifyComponent
  ],
  templateUrl: './generic-form.component.html',
  styleUrl: './generic-form.component.css'
})
export class GenericFormComponent {

  formData: FormGroup<any>;

  constructor(private fb: NonNullableFormBuilder) {
    this.formData = this.fb.group({
      id: ['', [Validators.required]],
      name: ['']
    })
  }
}
