import { Component } from '@angular/core';
import { CreateButtonComponent } from '@site/app/components/bpmn-toolbar/create-button/create-button.component';
import { SaveButtonComponent } from '@site/app/components/bpmn-toolbar/save-button/save-button.component';
import { PreviewButtonComponent } from '@site/app/components/bpmn-toolbar/preview-button/preview-button.component';

@Component({
  selector: 'app-bpmn-toolbar',
  standalone: true,
  imports: [
    CreateButtonComponent,
    SaveButtonComponent,
    PreviewButtonComponent
  ],
  templateUrl: './bpmn-toolbar.component.html',
  styleUrl: './bpmn-toolbar.component.css'
})
export class BpmnToolbarComponent {

}
