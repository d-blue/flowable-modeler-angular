import { Component } from '@angular/core';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { BpmnModelerService } from '@site/app/service/bpmn-modeler.service';

@Component({
  selector: 'app-create-button',
  standalone: true,
  imports: [
    NzIconModule
  ],
  templateUrl: './create-button.component.html',
  styleUrl: './create-button.component.css'
})
export class CreateButtonComponent {

  constructor(private bpmnModelerService: BpmnModelerService) {
  }

  createNewDiagram() {
    this.bpmnModelerService.createNewDiagram().then(() => {
    })
  }
}
