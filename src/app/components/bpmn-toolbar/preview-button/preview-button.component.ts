import { Component, ViewChild } from '@angular/core';
import { NzIconModule } from 'ng-zorro-antd/icon';
import {
  PreviewModalComponent
} from '@site/app/components/bpmn-toolbar/preview-button/preview-modal/preview-modal.component';

@Component({
  selector: 'app-preview-button',
  standalone: true,
  imports: [
    NzIconModule,
    PreviewModalComponent
  ],
  templateUrl: './preview-button.component.html',
  styleUrl: './preview-button.component.css'
})
export class PreviewButtonComponent {
  @ViewChild('previewModalComponent') previewModalComponent?: PreviewModalComponent;

  showPreviewModal(): void {
    this.previewModalComponent?.showModal();
  }
}
