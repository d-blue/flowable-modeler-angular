import { Component } from '@angular/core';
import { MarkdownModule } from 'ngx-markdown';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzModalComponent, NzModalModule } from 'ng-zorro-antd/modal';
import { BpmnModelerService } from '@site/app/service/bpmn-modeler.service';

@Component({
  selector: 'app-preview-modal',
  standalone: true,
  imports: [
    NzButtonModule,
    NzModalModule,
    MarkdownModule
  ],
  templateUrl: './preview-modal.component.html'
})
export class PreviewModalComponent {
  isVisible = false;
  data: string = '';
  loading = false;

  constructor(private bpmnModelerService: BpmnModelerService) {}

  showModal(): void {
    this.isVisible = true;
    this.data = '';
    this.loading = true;
    this.bpmnModelerService.generateXml().subscribe({
      next: (xml) => {
        this.data = '```xml\n' + xml + '\n```';
      },
      complete: () => {
        this.loading = false;
      }
    })
  }

  handleOk(): void {
    this.isVisible = false;
  }

  handleCancel(): void {
    this.isVisible = false;
  }
}
