import { Component } from '@angular/core';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { IconifyComponent } from '@site/app/components/icon/iconify/iconify.component';

@Component({
  selector: 'app-save-button',
  standalone: true,
  imports: [
    NzIconModule,
    IconifyComponent
  ],
  templateUrl: './save-button.component.html',
  styleUrl: './save-button.component.css'
})
export class SaveButtonComponent {

}
