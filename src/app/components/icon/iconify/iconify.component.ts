import { Component, ElementRef, Input, OnChanges, OnInit, Renderer2, SimpleChanges, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'iconify',
  standalone: true,
  imports: [],
  templateUrl: './iconify.component.html',
  styleUrl: './iconify.component.css'
})
export class IconifyComponent implements OnInit, OnChanges {

  @ViewChild('iconContainer', {static: true}) private iconContainer?: ElementRef;
  @Input() customClass: string = "";

  @Input()
  set icon(icon: string) {
    const sections = icon.split(':');
    if (sections.length == 2) {
      this.prefix = sections[0];
      this.name = sections[1];
    }
  }

  @Input('color') color!: string;
  raw?: string
  prefix?: string;
  name?: string;

  ngOnInit(): void {
    this.loadSvgRaw();
  }

  constructor(private http: HttpClient, private render: Renderer2) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['icon']) {
      this.loadSvgRaw();
    }
  }



  loadSvgRaw() {
    const url = `https://api.iconify.design/${this.prefix}/${this.name}.svg?`

    this.http.get(url, {responseType: 'text'}).subscribe(text => {
      this.raw = text;
      const hostElement = this.iconContainer?.nativeElement;
      this.render.setProperty(hostElement, 'innerHTML', this.raw);
    })
  }

}
