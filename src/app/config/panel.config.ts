export interface ModdleElementPanelConfig {
  header: string
  iconType: 'iconify' | 'antd' | 'svg' | 'img',
  icon: string
}

export interface PanelConfig {
  [key: string]: ModdleElementPanelConfig
}

// https://icon-sets.iconify.design/bpmn

export const panelConfigs: PanelConfig = {
  'bpmn:Process': {
    header: 'Process',
    iconType: 'iconify',
    icon: 'codicon:server-process'
  },
  'bpmn:StartEvent': {
    header: 'Start Event',
    iconType: 'iconify',
    icon: 'bpmn:start-event'
  },
  'bpmn:EndEvent': {
    header: 'End Event',
    iconType: 'iconify',
    icon: 'bpmn:end-event'
  },
  'bpmn:IntermediateEvent': {
    header: 'Intermediate Event',
    iconType: 'iconify',
    icon: 'bpmn:intermediate-event'
  },
  'bpmn:UserTask': {
    header: 'User Task',
    iconType: 'iconify',
    icon: 'bpmn:user'
  }
}
