import { BpmnModelerService } from '@site/app/service/bpmn-modeler.service';
import { APP_INITIALIZER } from '@angular/core';

export function initializeApp(bpmnModelerService: BpmnModelerService): Function {
  return () => bpmnModelerService.initModeler();
}

export function provideBpmnModeler() {
  return {
    provide: APP_INITIALIZER,
    useFactory: initializeApp,
    multi: true,
    deps: [BpmnModelerService],
  }
}
