import PaletteProvider, { Translate } from 'bpmn-js/lib/features/palette/PaletteProvider';
import Palette from 'diagram-js/lib/features/palette/Palette';
import Create from 'diagram-js/lib/features/create/Create';
import ElementFactory from 'diagram-js/lib/core/ElementFactory';
import SpaceTool from 'diagram-js/lib/features/space-tool/SpaceTool';
import LassoTool from 'diagram-js/lib/features/lasso-tool/LassoTool';
import HandTool from 'diagram-js/lib/features/hand-tool/HandTool';
import GlobalConnect from 'diagram-js/lib/features/global-connect/GlobalConnect';
import { PaletteEntries } from 'diagram-js/lib/features/palette/PaletteProvider';

export default class CustomPaletteProvider extends PaletteProvider {

  static override $inject = [
    'palette',
    'create',
    'elementFactory',
    'spaceTool',
    'lassoTool',
    'handTool',
    'globalConnect',
    'translate'
  ];

  constructor(private palette: Palette, private create: Create, private elementFactory: ElementFactory,
              private spaceTool: SpaceTool, private lassoTool: LassoTool, private handTool: HandTool,
              private globalConnect: GlobalConnect, private translate: Translate) {

    super(palette, create, elementFactory, spaceTool, lassoTool, handTool, globalConnect, translate);
  }

  override getPaletteEntries(): PaletteEntries {
    console.log(this.palette);
    return super.getPaletteEntries();
  }
}

