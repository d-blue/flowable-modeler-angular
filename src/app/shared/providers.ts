import CustomPaletteProvider from '@site/app/shared/custom/CustomPaletteProvider';
import CustomRuleProvider from '@site/app/shared/rules/CustomRuleProvider';

export const CustomProviders = [
  {
    __init__: [],
    paletteProvider: ['type', CustomPaletteProvider],
    bpmnRules: ['type', CustomRuleProvider]
  }
]
