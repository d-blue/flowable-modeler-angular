import EventBus from 'diagram-js/lib/core/EventBus';
import BpmnRules from 'bpmn-js/lib/features/rules/BpmnRules';


export default class CustomRuleProvider extends BpmnRules {

  static override $inject = [
    'eventBus'
  ];

  constructor(private eventBus: EventBus) {
    super(eventBus);
  }

  override init() {
    super.init();
  }
}
