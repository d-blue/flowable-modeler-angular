import { ModdleElement } from 'bpmn-js/lib/model/Types';

export interface ElementChangeEvent {
  type: string;
  element: ModdleElement;
}
