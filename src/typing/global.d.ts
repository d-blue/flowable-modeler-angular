import { Connection, Root, Shape, Label, Parent } from 'bpmn-js/lib/model/Types'
import { ModdleElement } from 'bpmn-js/lib/model/Types';

declare global {
  type BpmnElement = Root | Shape | Connection | Label | Parent | ModdleElement
}
