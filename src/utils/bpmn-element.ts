export function getElementType(element: BpmnElement): string {
  return element.businessObject?.$type || element.type || element.$type
}
